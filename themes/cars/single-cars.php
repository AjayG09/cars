<?php get_header(); ?>
<main>	
	<div class="cars">
		<?php the_post(); ?>	
		<article>
			<p><?php the_field('cars_description'); ?></p>
			<div class="thumbnail"><?php the_post_thumbnail('full'); ?></div>
		</article>
	</div>	
</main>
<?php get_footer(); ?>