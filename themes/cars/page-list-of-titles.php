<?php get_header(); ?>
<main>
<?php $loop = new WP_Query(array('post_type'=>'cars')); ?>
<div class="cars-listing">
	<?php if($loop->have_posts()):while($loop->have_posts()):$loop->the_post(); ?>
		<?php if(get_the_title()): ?>
			<h3><?php the_title(); ?></h3>
		<?php endif; ?>
	<?php endwhile; ?>
	<?php else : ?>
	<h2> There are no posts.</h2> 
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
</div>
</main>
<?php get_footer(); ?>