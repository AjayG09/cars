<?php 

/******
* custom post type 
*******/
function create_post_type() {
  register_post_type( 'cars',
    array(
      'labels' => array(
        'name' => __( 'cars' ),
        'singular_name' => __( 'car' )
      ),
      'taxonomies' => array('category'), 
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'has_archive' => true,
      'rewrite' => false,
      'supports' => array( 'title', 'thumbnail' ),
    )
  ); 
}
add_action( 'init', 'create_post_type' );


/******
* Add Theme Support
*******/

function my_theme_setup() {
  add_theme_support('menus');
  add_theme_support( 'post-thumbnails' );
  register_nav_menu('primary', 'primary header navigation');

}

add_action( 'init', 'my_theme_setup' ); 


/*
* add excerpt support for custom post type
*
*/
function wpcodex_add_excerpt_support_for_cpt() {
 add_post_type_support( 'post-type-alias', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_cpt' );

/*
* Add Custom excerpt length
*/
function custom_excerpt_length(){
 return 50;
}
add_filter('excerpt_length','custom_excerpt_length');

add_post_type_support( 'cars', 'excerpt' );


/*
* Add customizer logo image
*/
function theme_customize_register_logo_image( $wp_customize ) {
  $wp_customize->add_section('themename_color_scheme', array( 
    'title' => __('header logo image', 'fresh'),
    'priority' => 121,
  ));


  $wp_customize->add_setting('themename_theme_options', array(
        'default'    => 'image.jpg',
        'capability' => 'edit_theme_options',
        //'type'       => 'option',
        'title'      => 'Test',
    ));
 
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'themename_theme_options', array(
        'label'    => __('Image Upload Test', 'fresh'),
        'section'  => 'themename_color_scheme',
        'settings' => 'themename_theme_options',
    )));
 

}
add_action('customize_register','theme_customize_register_logo_image');



/*
* Category of grade a,grade b and grade c filter
*/
add_filter( 'the_title', 'category_filter' );

function category_filter( $title ) {
  if(is_page('list-of-titles')){
    if(has_category('grade-a') && has_category('grade-b') && has_category('grade-c')){
      return $title;
  }
  }else { return $title; }  
}


/*
* Custom Excerpt function for category c and d
*/
function custom_field_excerpt() {
  global $post;
  $text = get_field('cars_description'); 
  if ( '' != $text && has_category('grade-c') && has_category('grade-d')) {
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]&gt;', ']]&gt;', $text);
    $excerpt_length = 50; 
    $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
    $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
  }
  return apply_filters('the_excerpt', $text);
}  


function excerpt_for_a_grade() {
  global $post;
  $text = get_field('cars_description'); 
  if ( '' != $text ) {
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]&gt;', ']]&gt;', $text);
    $excerpt_length = 50; // 20 words
    $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
    $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
  }
  return apply_filters('the_excerpt', $text);
}
?>