<?php get_header(); ?>
<main>
<?php $loop = new WP_Query(array('post_type'=>'cars', 'posts_per_page' => '3', 'orderby' => 'rand')); ?>
<div class="cars-listing">
	<h2><?php wp_title(); ?></h2>
	<ul class="list">
		<?php if($loop->have_posts()):while($loop->have_posts()):$loop->the_post(); ?>
		<li>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<p>
				<?php 
					$custom_field_excerpt = custom_field_excerpt(); 
					echo $custom_field_excerpt; 
				?>
			</p>
			<div class="thumbnail"><?php the_post_thumbnail('thumbnail'); ?></div>	
		</li>
		<?php endwhile; ?>
		<?php else : ?>
		<h2> There are no posts.</h2> 
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	</ul>	
</div>
</main>
<?php get_footer(); ?>