<?php get_header(); ?>
<main>
<?php $loop = new WP_Query(array('post_type'=>'cars','posts_per_page' => '3', 'orderby' => 'rand')); ?>
<div class="cars-listing">
	<h2>Cars Listing</h2>
	<ul>
		<?php if($loop->have_posts()):while($loop->have_posts()):$loop->the_post(); ?>
		<li class="list">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<p><?php the_field('cars_description'); ?></p>
			<div class="thumbnail"><?php the_post_thumbnail(); ?></div>
		</li>
		<?php endwhile; ?>
		<?php else : ?>
		<h2> There are no posts.</h2> 
		<?php endif; ?>
	</ul>	
</div>
</main>
<?php get_footer(); ?>