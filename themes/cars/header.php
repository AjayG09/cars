<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php wp_title(); ?></title>
	<?php wp_head(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body <?php body_class(); ?>>
	<header>
		<div class="wrapper cf">
			<!-- Logo -->
			<h1 class="inline-block">
			<?php if(get_theme_mod('themename_theme_options')) { ?>
			<?php
				$get_image_id = attachment_url_to_postid(get_theme_mod('themename_theme_options'));
				$alt = get_post_meta ( $get_image_id, '_wp_attachment_image_alt', true );
			?>
				<a href="#FIXME" title="<?php echo $alt; ?>">
					<img src="<?php echo esc_url(get_theme_mod('themename_theme_options','image.jpg')); ?>" alt="<?php echo $alt; ?>">
				</a>	
				<?php } else {?>
				<a href="#FIXME" title="<?php echo $img['alt']; ?>"">
					<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
				</a>
			<?php }?>
			<!-- logo ends  -->
			</h1>
			<!-- nav starts here -->          
          	<nav class="navbar cf">
            	<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
            </nav>	
		</div>
	</header>