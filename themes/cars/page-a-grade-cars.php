<?php get_header(); ?>
<main>
<?php $loop = new WP_Query(array('post_type'=>'cars', 'category_name' => 'grade-a')); ?>
<div class="cars-listing">
	<?php if($loop->have_posts()):while($loop->have_posts()):$loop->the_post(); ?>
		<div class="list">
			<h3><?php the_title(); ?></h3>
			<p><?php $temp = excerpt_for_a_grade(); echo $temp; ?></p>
			<span><?php echo get_the_date('F j, Y'); ?></span>	
			<div class="thumbnail"><?php the_post_thumbnail('thumbnail'); ?></div>	
			<a href="<?php the_permalink(); ?>">Read More</a>	
		</div>	
	<?php endwhile; ?>
	<?php else : ?>
	<h2> There are no posts.</h2> 
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
</div>
</main>
<?php get_footer(); ?>
